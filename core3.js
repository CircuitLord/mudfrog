//Cool code from other people who are smarter than me.
const Discord = require("discord.js");
const Enmap = require("enmap");
const json = require('jsonfile');
const fs = require("fs");


//MUDFROG!!!
const MUDFROG = new Discord.Client();


//Initialize subobjects on MUDFROG:
MUDFROG.config = require("./config.json");
MUDFROG.configTemp = require("./configTemplate.json");
MUDFROG.activeGuilds = {};
MUDFROG.utils = require(`./utils/circuit-utils.js`);


MUDFROG.guildConfigs = new Enmap({
    persistent: true,
    name: `guildConfigs`,
    fetchAll: true,
    autoFetch: true
  });



//Initialize Mudfrog's abilities:

//Stop the feelin'.
MUDFROG.stop = function(error) {
    if (error) {
        console.log(`Bot shutting down... Error: ${error}`);
    } else {
        console.log(`Bot shutting down...`);
    }
    process.exit();
}

//Allows for compatibility with my future evil plans.
MUDFROG.runCommand = function(MUDFROG, msg, command) {
    require(`./commands/${command}.js`).run(MUDFROG, msg);
}

//Load all the info Mudfrog knows about you:
MUDFROG.loadConfigs = async function() {

    //Wait for enmap to load.
    await MUDFROG.guildConfigs.defer;

    //Clear configs:
    MUDFROG.activeGuilds = {};
    var guildCount = 0;
    
    function testGuildConfig(guild, index, arr) {
        guildCount++;
        //If config doesn't exist, make a new one:
        console.log(MUDFROG.guildConfigs.get(guild.id))
        if (MUDFROG.guildConfigs.get(guild.id) === undefined) {
            console.log("Making new guildconfig: " + MUDFROG.configTemp);
            MUDFROG.guildConfigs.set(guild.id, MUDFROG.configTemp);
            //TODO: new guild config on server join
        }
        //Load the config:

        MUDFROG.activeGuilds[guild.id] = MUDFROG.guildConfigs.get(guild.id);
        return true;
    }

    MUDFROG.guilds.every(testGuildConfig);
    //console.log(MUDFROG.activeGuilds)
    
    console.log(`Loaded ${guildCount} configs...`);

}

//Reload specfic server config:
MUDFROG.reloadConfig = function(key) {
    MUDFROG.activeGuilds[key] = MUDFROG.guildConfigs.get(key);
}

//Returns roles from author in array:
MUDFROG.getRolesFromMsg = function(msg) {
    var roles = [];
    function findRoles(role, index, arr) {
        roles.push(role);
        return true;
    }

    msg.member.roles.every(findRoles);
    return roles;
}











//If none of that crashes:
MUDFROG.on("ready", function (msg) {

    //Load server configs into memory.
    MUDFROG.loadConfigs();

    //Let the owner know what's up.
    console.log(`Mudfrog ${MUDFROG.config.botVersion} is ready to go.`);
});




MUDFROG.on("message", function (msg) {

    //Ignore DMs and Bots.
    if (msg.author.bot == true) return;
    if (msg.channel.type === "dm") return;

    //ECTOR stuff
    require(`./utils/ector.js`).run(MUDFROG, msg)


    //Test if message is a command.
    msg.isCommand = false;
    if (msg.content.startsWith(MUDFROG.activeGuilds[msg.guild.id].prefix)) msg.isCommand = true;

    //If msg is not a command, return.
    if (msg.isCommand == false) return;



    //Addons to message object:
    msg.guildConfig = MUDFROG.activeGuilds[msg.guild.id];
    msg.author.roles = MUDFROG.getRolesFromMsg(msg);
    msg.command = msg.content.substr(1).split(" ")[0].toLowerCase();
    msg.args = msg.content.substr(1).split(" ").slice(1);


    
    //Command Handler.
    const commandList = [];
    fs.readdirSync("./commands/").forEach(file => {
        commandList.push(file);
    });


//Random message 8ball command:
    if (msg.command == "8" || msg.command == "8ball") {
        MUDFROG.runCommand(MUDFROG, msg, "8");
        return;
    }

    if (msg.command == "bridgeSpectrum" || msg.command == "bridge") {
        require(`./utils/spectrumBridge.js`).run(MUDFROG, msg)
        return;
    }



//Stop command:
    if (msg.command == `stop` || msg.command == `s` && msg.author.id == `202177349867929600`) {
        MUDFROG.stop();
    }




    //If custom command is not detected, fallback to automatic detection. (deprecated)
    if (commandList.includes(`${msg.command}.js`)) {
        MUDFROG.runCommand(MUDFROG, msg, msg.command);
        return;
    }



    






});

MUDFROG.login(MUDFROG.config.testToken);

