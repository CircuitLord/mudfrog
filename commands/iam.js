






exports.addRoleToUser = function(MUDFROG, msg) {
	console.log("role requested: " + msg.args[0]);
	if (msg.guildConfig.modules.addRole.roles[msg.args[0].toLowerCase()] == undefined) {
		msg.channel.send("Sorry, you can't add that role.");
		return;
	}
	let roleToAdd = msg.guild.roles.get(msg.guildConfig.modules.addRole.roles[msg.args[0].toLowerCase()].id);
	msg.member.addRole(roleToAdd).catch(console.error);
	msg.channel.send(`Okay, I added the ${roleToAdd.name} role to you.`);

};

exports.removeRoleFromUser = function(MUDFROG, msg) {
	if (msg.guildConfig.modules.addRole.roles[msg.args[0]] == undefined) {
		msg.channel.send("Sorry, you can't remove that role.");
		return;
	}
	let roleToRemove = msg.guild.roles.get(msg.guildConfig.modules.addRole.roles[msg.args[0].toLowerCase()].id);
	msg.member.removeRole(roleToRemove).catch(console.error);
	msg.channel.send(`Okay, I removed ${roleToRemove.name} from you.`);

};



exports.listRoles = function(MUDFROG, msg) {

	var rolesMsg = "";

	Object.keys(msg.guildConfig.modules.addRole.roles).forEach(function(key) {
		rolesMsg = `${rolesMsg}\n${msg.guildConfig.modules.addRole.roles[key].name}`;
	});

	msg.channel.send(`The roles you can add are:\n\`\`\`${rolesMsg}\`\`\``);




};






exports.removeRoleFromList = function(MUDFROG, msg, role) {
	if (role === null) return;

	var roleIndex = msg.guildConfig.modules.addRole.roles.indexOf(role.name.toLowerCase());
	msg.guildConfig.modules.addRole.roles.splice(roleIndex, roleIndex + 1);

	MUDFROG.guildConfigs.set(msg.guild.id, msg.guildConfig);
	MUDFROG.reloadConfig(msg.guild.id);
	msg.channel.send(`I removed ${role.name} from the list. :0.`);

};



exports.addRoleToList = function(MUDFROG, msg, role) {
	if (role === null) return;


	msg.guildConfig.modules.addRole.roles[role.name.toLowerCase()] = {
		"name": role.name.toLowerCase(),
		"id": role.id,
		"whoCan": 0 //role.whoCan
	};



	MUDFROG.guildConfigs.set(msg.guild.id, msg.guildConfig);
	MUDFROG.reloadConfig(msg.guild.id);
	console.log(MUDFROG.guildConfigs.get(msg.guild.id).modules.addRole);
	msg.channel.send(`I added ${role.name} to the list. Use \`-iam ${role.name}\` to add it to yourself.`);
};