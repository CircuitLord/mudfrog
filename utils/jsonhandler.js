const fs = require("fs");


exports.readJSON = function(filepath) {
	try {
		let rawfile = fs.readFileSync(filepath);
		let json = JSON.parse(rawfile);
		return json;
	} catch(err) {
		console.log(err);
	}

}


exports.writeJSON = function(filepath, object) {
	try {
		fs.writeFileSync(JSON.stringify(object));
	} catch (err) {
		console.log(err);
	}
}

