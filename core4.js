//Cool code from other people who are smarter than me.
const Discord = require(`discord.js`);
const Enmap = require(`enmap`);
const fs = require("fs");
var Filter = require('bad-words-relaxed');
var filter = new Filter();
//var Ector = require('ector');
//var ector = new Ector();

filter.removeWords('space');
filter.removeWords('assessing');
filter.removeWords('screw');
filter.removeWords('robertsspaceindustries');
filter.removeWords('suspicious');
//MUDFROG!!!
const MUDFROG = new Discord.Client();


//Initialize subobjects on MUDFROG:
MUDFROG.config = require(`./config.json`);
MUDFROG.configTemp = require(`./configTemplate.json`);
MUDFROG.activeGuilds = {};
MUDFROG.utils = require(`./utils/circuit-utils.js`);
MUDFROG.json = require(`./utils/jsonhandler.js`);


MUDFROG.guildConfigs = new Enmap({
	persistent: true,
	name: `guildConfigs`,
	fetchAll: true,
	autoFetch: true
});

MUDFROG.xp = new Enmap({
	persistent: true,
	name: `xp`,
	fetchAll: true,
	autoFetch: true
});

MUDFROG.xp.usersGot = [];


//The XP looper timer thingy.
function xpTimer() {
	MUDFROG.xp.usersGot = [];

	setTimeout(xpTimer, 60000);
}

xpTimer();


//Initialize Mudfrog's abilities:

//Stop the feelin'.
MUDFROG.stop = function(error) {
	if (error) {
		console.log(`Bot shutting down... Error: ${error}`);
	} else {
		console.log(`Bot shutting down...`);
	}
	process.exit();
};

//Allows for compatibility with my future evil plans.
MUDFROG.runCommand = function(MUDFROG, msg, command) {
	require(`./commands/${command}.js`).run(MUDFROG, msg);
};

//Load all the info Mudfrog knows about you:
MUDFROG.loadConfigs = async function() {

	//Wait for enmap to load.
	await MUDFROG.guildConfigs.defer;

	//Clear configs:
	MUDFROG.activeGuilds = {};
	var guildCount = 0;

	function testGuildConfig(guild) {
		guildCount++;
		//If config doesn't exist, make a new one:
		MUDFROG.guildConfigs.ensure(guild.id, MUDFROG.configTemp);

		//Load the config:
		MUDFROG.activeGuilds[guild.id] = MUDFROG.guildConfigs.get(guild.id);
		return true;
	}

	MUDFROG.guilds.every(testGuildConfig);

	console.log(`Loaded ${guildCount} configs...`);

};

//Reload specfic server config:
MUDFROG.reloadConfig = function(guildID) {
	MUDFROG.activeGuilds[guildID] = MUDFROG.guildConfigs.get(guildID);
};

//Returns roles from author in array:
MUDFROG.getRolesFromMsg = function(msg) {
	var roles = [];

	function findRoles(role, index, arr) {
		roles.push(role);
		return true;
	}

	msg.member.roles.every(findRoles);
	return roles;
};



//If none of that crashes:
MUDFROG.on("ready", async function(msg) {



	//Load server configs into memory.
	await MUDFROG.loadConfigs();

	//Let the owner know what's up.
	console.log(`Mudfrog ${MUDFROG.config.botVersion} is ready to go.`);
});



MUDFROG.on("message", function(msg) {

	//Ignore DMs and Bots.
	if (msg.author.bot || msg.channel.type === "dm") return;


	//Test if message is a command.
	msg.isCommand = false;
	if (msg.content.startsWith(MUDFROG.activeGuilds[msg.guild.id].prefix)) msg.isCommand = true;


	msg.guildConfig = MUDFROG.activeGuilds[msg.guild.id];

	//Run any modules that run without it being a command.

	//Badwords No More
	if (msg.guildConfig.modules.noBadWords.enabled && filter.isProfane(msg.content)) {
		try {
			msg.delete();
		} catch (e) {
			console.log(e);
		}

		//Remove the bad words entirely.
		if (msg.guildConfig.modules.noBadWords.settings.bleepOrRemove === true) {
			var newMsg = filter.clean(msg.content);
			msg.channel.send(`_${msg.member.displayName} said:_ ${newMsg}`);
		}


		//TODO:
		//replace with happy words


		return;
	}





	//XP
	msg.xpID = msg.guild.id + "-" + msg.author.id;
	if (!MUDFROG.xp.usersGot.includes(msg.xpID) && MUDFROG.activeGuilds[msg.guild.id].modules.xp.enabled) {

		//Add user to the whitelist for XP.
		MUDFROG.xp.usersGot.push(msg.xpID);

		let currXP = MUDFROG.xp.ensure(msg.guild.id, {});

		if (currXP[msg.author.id] == null) {
			currXP = 0;
		} else {
			currXP[msg.author.id] += MUDFROG.activeGuilds[msg.guild.id].modules.xp.XPIncrement;
		}

		MUDFROG.xp.set(msg.guild.id, currXP);

	}



	//If msg is not a command, return.
	if (msg.isCommand == false) return;



	//Addons to message object:
	msg.author.roles = MUDFROG.getRolesFromMsg(msg);
	msg.command = msg.content.substr(1).split(" ")[0].toLowerCase();
	msg.args = msg.content.substr(1).split(" ").slice(1);



	//Command Handler.
	const commandList = [];
	fs.readdirSync("./commands/").forEach(file => {
		commandList.push(file);
	});


	//Random message 8ball command:
	if (msg.command == "8" || msg.command == "8ball") {
		MUDFROG.runCommand(MUDFROG, msg, "8");
		return;
	}

	//change settings
	if (msg.command == "settings" && msg.author.id == `202177349867929600`) {
		//TODO: expand to new file

		if (msg.args[0] == "enable") {

			if (msg.args[1] == "noBadWords" || msg.args[1] == "nobadwords") {
				try {
					msg.guildConfig.modules.noBadWords.enabled = true;
					MUDFROG.guildConfigs.set(msg.guild.id, msg.guildConfig);
					MUDFROG.reloadConfig(msg.guild.id);
					msg.channel.send(`The module No Bad Words has been enabled.`);
				} catch (e) {
					console.log(e);
				}
			}



		}

		if (msg.args[0] == "disable") {


			if (msg.args[1] == "noBadWords" || msg.args[1] == "nobadwords") {
				try {
					msg.guildConfig.modules.noBadWords.enabled = false;
					MUDFROG.guildConfigs.set(msg.guild.id, msg.guildConfig);
					MUDFROG.reloadConfig(msg.guild.id);
					msg.channel.send(`The module No Bad Words has been disabled.`);
				} catch (e) {
					console.log(e);
				}
			}

		}

	}


	//IAM role commands

	//Add a role to a user.

	if ("iamnot" == msg.command && msg.guildConfig.modules.addRole.enabled) {
		if (msg.args == null || msg.args == "") {
			msg.channel.send("Please specify the role you'd like to remove.");
			return;
		} else {
			require("./commands/iam.js").removeRoleFromUser(MUDFROG, msg);
			return;
		}
	}

	if ("iam" == msg.command && msg.guildConfig.modules.addRole.enabled) {

		if (msg.args == null || msg.args == "") {
			require("./commands/iam.js").listRoles(MUDFROG, msg);
			return;
		} else {
			require("./commands/iam.js").addRoleToUser(MUDFROG, msg);
			return;
		}

	}


	//Add a role to a user.
	if ("addroletolist" == msg.command && msg.guildConfig.modules.addRole.enabled && msg.author.id == `202177349867929600`) {

		if (msg.args[0] == null) return;
		let roleToAdd = msg.guild.roles.find(role => role.name.toLowerCase() === msg.args[0].toLowerCase());

		require("./commands/iam.js").addRoleToList(MUDFROG, msg, roleToAdd);

	}

	//Add a role to a user.
	if ("removerolefromlist" == msg.command && msg.guildConfig.modules.addRole.enabled && msg.author.id == `202177349867929600`) {

		if (msg.args[0] == null) return;
		let roleToRemove = msg.guild.roles.find(role => role.name.toLowerCase() === msg.args[0].toLowerCase());
		console.log("Okay. Removing...")
		require("./commands/iam.js").removeRoleFromList(MUDFROG, msg, roleToRemove);

	}

	//Delete messages.
	if ("delete" == msg.command && msg.author.id == `202177349867929600`) {

		if (msg.args[0] == null) return;
		let msgsToDelete = msg.args[0];

		msg.channel.bulkDelete(msgsToDelete).then(() => {
			msg.channel.send("Deleted " + msgsToDelete.toString() + " messages.").then(msg => msg.delete(5000));
		});

		return;
	}



	//Stop command:
	if (msg.command == `stop` || msg.command == `s` && msg.author.id == `202177349867929600`) {
		MUDFROG.stop();
	}

	//spectrum bridge command:
	if (msg.command == `spec` && msg.author.id == `202177349867929600`) {
		require("./commands/spectrum.js").run(MUDFROG, msg);

	}



	//If custom command is not detected, fallback to automatic detection. (deprecated)
	if (commandList.includes(`${msg.command}.js`)) {
		MUDFROG.runCommand(MUDFROG, msg, msg.command);
		return;
	}



});

MUDFROG.login(MUDFROG.config.liveToken2);