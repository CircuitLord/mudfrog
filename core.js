const Discord = require("discord.js");
var json = require('json-file');
const fs = require("fs");
const jf = require("jsonfile");



const MUDFROG = new Discord.Client();





//Initialize all the sub parts of the MUDFROG core object.
MUDFROG.utils = require("./utils/circuit-utils.js");
MUDFROG.config = require("./config.json");
MUDFROG.configTemp = require("./configTemplate.json")
MUDFROG.db = "test";

MUDFROG.activeConfigs = {};



MUDFROG.stop = function() {
    console.log("Bot shutting down...");
    process.exit();
}

MUDFROG.runCommand = function(MUDFROG, msg, command) {
    require(`./commands/${command}.js`).run(MUDFROG, msg);
}

MUDFROG.loadConfigs = function() {
    function testGuildConfig(guild, index, arr) {
        if (MUDFROG.db.configGet(guild.id) === null) {
            MUDFROG.db.configSet(guild.id, MUDFROG.configTemp);
        }
        return true;
    }

    MUDFROG.guilds.every(testGuildConfig);

    MUDFROG.activeConfigs = MUDFROG.db.loadConfigs();

}


MUDFROG.reloadConfig = function(key) {
    MUDFROG.activeConfigs[key] = MUDFROG.db.configGet(key);
}




//When Mudfrog starts:

MUDFROG.on("ready", function (msg) {

    //Load server configs.
    MUDFROG.loadConfigs();


    console.log(`Mudfrog ${MUDFROG.config.botVersion} is ready to go.`);
});



MUDFROG.on("message", function (msg) {

    //Ignore DMs and Bots.
    if (msg.author.bot == true) return;
    if (msg.channel.type === "dm") return;


    

    //Test if message is a command.
    msg.isCommand = false;
    if (msg.content.startsWith(MUDFROG.activeConfigs[msg.guild.id].prefix)) msg.isCommand = true;

    //If msg is a command, return.
    if (msg.isCommand == false) return;



    



    //Useful addons to MSG object.
    msg.serverConfig = MUDFROG.activeConfigs[msg.guild.id];
    msg.username = msg.author.username;
    msg.author.roles = MUDFROG.utils.getRoles(msg);
    msg.command = msg.content.substr(1).split(" ")[0].toLowerCase();
    msg.args = msg.content.substr(1).split(" ").slice(1);

    console.log(msg.command);

    //Command Handler.
    const commandList = [];
    fs.readdirSync("./commands/").forEach(file => {
        commandList.push(file);
    });


    if (msg.command == "8" || msg.command == "8ball") {
        MUDFROG.runCommand(MUDFROG, msg, "8");
        return;
    }

    if (msg.command == "t" || msg.command == "translate") {
        //console.log("translating")
        MUDFROG.runCommand(MUDFROG, msg, "t");
        return;
    }




//Reboot the bot.
    if (msg.command == "reboot" && msg.username == "CircuitLord") {
        MUDFROG.stop();
    }




    //If custom command is not detected, fallback to automatic detection.
    if (commandList.includes(`${msg.command}.js`)) {
        MUDFROG.runCommand(MUDFROG, msg, msg.command);
        return;
    }

});


/*
if (MUDFROG.config.current == "test") {
    MUDFROG.login(MUDFROG.config.test);
}

if (MUDFROG.config.current == "live") {
    MUDFROG.login(MUDFROG.config.live);
}
*/

MUDFROG.login(MUDFROG.config.liveToken);
